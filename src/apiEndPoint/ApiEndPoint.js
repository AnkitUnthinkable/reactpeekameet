import axios from "axios";

export const authentication = authData => {
  return new Promise(function(resolve, reject) {
    resolve(
      axios.post(
        "https://apipeekameet.cloudzmall.com/peekameet/api/v1/public/user/login",
        authData
      )
    );
  });
};

export const getUserProfileData = payLoad => {
  return new Promise(function(resolve, reject) {
    resolve(
      axios.get(
        "https://apipeekameet.cloudzmall.com/peekameet/api/v1/user/nearby",
        payLoad
      )
    );
  });
};

export const getNoteData = payLoad => {
  return new Promise(function(resolve, reject) {
    resolve(
      axios.get(
        "https://apipeekameet.cloudzmall.com/peekameet/api/v1/followUpNotes",
        payLoad
      )
    );
  });
};

export const deleteNotes = (noteId, payLoad1, payLoad2) => {
  return new Promise(function(resolve, reject) {
    resolve(
      axios.delete(
        "https://apipeekameet.cloudzmall.com/peekameet/api/v1/followUpNote/" +
          noteId,
        payLoad1,
        payLoad2
      )
    );
  });
};

export const addNotes = (payLoad, header) => {
  return new Promise(function(resolve, reject) {
    resolve(
      axios.post(
        "https://apipeekameet.cloudzmall.com/peekameet/api/v1/followUpNote",
        payLoad,
        header
      )
    );
  });
};

export const editNotes = (noteId, payLoad, header) => {
  return new Promise(function(resolve, reject) {
    resolve(
      axios.put(
        "https://apipeekameet.cloudzmall.com/peekameet/api/v1/followUpNote/"+noteId,
        payLoad,
        header
      )
    );
  });
};
