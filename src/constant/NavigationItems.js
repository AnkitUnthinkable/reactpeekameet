import imageHome from '../assets/home_icon.svg';
import imageContacts from "../assets/contact_icon.svg";
import imageScan from "../assets/scan_icon.svg";
import imageMovement from "../assets/movement_icon.svg";
import imageNotification from "../assets/notification_icon.svg";
import imageMessage from "../assets/message_icon.svg"; 



const navigationItems = {
    //Nav Items
    home: {
      imagePath: imageHome,
      message: "Home"
    },
    contacts: {
      imagePath: imageContacts,
      message: "Contacts"
    },
    message: {
      imagePath: imageMessage,
      message: "Message"
    },
    scan: { imagePath: imageScan, message: "Scan" },
    movement: { imagePath: imageMovement, message: "The Movement" },
    notification: { imagePath: imageNotification, message: "Notification" }
  };

  export default navigationItems;