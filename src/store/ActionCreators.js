import * as actionTypes from "./ActionTypes";
import axios from "axios";

export const Authentication = (email, password) => {
  return dispatch => {
    dispatch(authStart());
    const authData = { email: email, password: password };

    axios
      .post(
        "https://apipeekameet.cloudzmall.com/peekameet/api/v1/public/user/login",
        authData
      )
      .then(response => {
        localStorage.setItem("token", response.data.data[0].token);
        localStorage.setItem("id", response.data.data[0].customer._id);
          console.log("auth")
        dispatch(
          authSuccess(
            response.data.data[0].token,
            response.data.data[0].customer._id
          )
        );
      })
      .catch(err => {
        console.log(err);
        dispatch(authFail(err));
      });
  };
};

export const authSuccess = (token, userId) => {
  return {
    type: actionTypes.AUTH_SUCCESS,
    token: token,
    userId: userId
  };
};
export const authFail = error => {
  return {
    type: actionTypes.AUTH_FAIL,
    error: error
  };
};
export const authStart = () => {
  return {
    type: actionTypes.AUTH_START
  };
};

export const noteData = () => {
  let userId = localStorage.getItem("id");
  let token = localStorage.getItem("token");

  const createdFor = { createdFor: userId };
  const authorization = { Authorization: token };
  return dispatch => {
    axios
      .get(
        "https://apipeekameet.cloudzmall.com/peekameet/api/v1/followUpNotes",
        {
          query: { ...createdFor },
          headers: { ...authorization }
        }
      )
      .then(response => {
        return dispatch({ type: actionTypes.NOTE_DATA, noteData:response.data.data[0].docs });
      })
      .catch(err => {
        console.log(err);
        return { type: actionTypes.NOTE_DATA, noteData: err };
      });
  };
};
// export const deleteNote=(noteId)=>{
//   axios
//   .delete(
//     "https://apipeekameet.cloudzmall.com/peekameet/api/v1/followUpNote/" +
//       this.props.noteId,
//     { headers: { Authorization: localStorage.getItem("token") } },
//     { noteId:noteId }
//   )
//   .then(response => {
//     console.log(response);
    

//     this.props.history.push("/user/notes");

//   return{
//     type:actionTypes.DELETE_NOTE,
//     noteData:response
//   }
//   })
//   .catch(err => {
//     console.log(err);
//   });

// }