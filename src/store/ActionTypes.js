export const AUTH_START = "AUTH_START";
export const AUTH_SUCCESS = "AUTH_SUCCESS";
export const AUTH_FAIL = "AUTH_FAIL";
export const AUTH_LOGOUT='AUTH_LOGOUT';
export const NOTE_DATA='NOTE_DATA';
export const DELETE_NOTE='DELETE_NOTE';
