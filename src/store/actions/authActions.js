import * as actionTypes from "../ActionTypes";
import { authentication } from "../../apiEndPoint/ApiEndPoint";

export const Authentication = (email, password) => {
  return dispatch => {
    dispatch(authStart());
    const authData = { email: email, password: password };
    ///API CALL
    authentication(authData)
      .then(response => {
        localStorage.setItem("token", response.data.data[0].token);
        localStorage.setItem("id", response.data.data[0].customer._id);
        console.log("api fetching")
        dispatch(
          authSuccess(
            response.data.data[0].token,
            response.data.data[0].customer._id
          )
        );
      })
      .catch(err => {
        console.log(err.message);
        dispatch(authFail('Invalid Email'));
      });
  };
};

export const authStart = () => {
  return {
    type: actionTypes.AUTH_START
  };
};

export const authSuccess = (token, userId) => {
  return {
    type: actionTypes.AUTH_SUCCESS,
    token: token,
    userId: userId
  };
};

export const authFail = error => {
  return {
    type: actionTypes.AUTH_FAIL,
    error: error
  };
};

