import React from 'react';

const Element = (props) => {

    return (
        <input type={props.type} placeholder={props.placeholder} onChange={props.change} value={props.value} />
    );
};

export default Element