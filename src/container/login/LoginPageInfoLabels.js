import React from 'react'
import './LoginPage.css'
import Button from './button/Button';
const LoginPageInfoLabels = () => {
    return (
        <div>
            <p className="PEEKaMEET-lets-you-n"><big><b>PEEKaMEET</b></big> lets you network more effectively to achieve your business and career goals</p><br/>
            <div className="button-container">
                <Button name="Freelancer" style="button1" />
                <Button name="Job Seeker" style="button2" />
                <Button name="Enterpreneur" style="button3" />
                <Button name="Mompreneur" style="button4" />
                <Button name="Internship Seeker" style="button5" />
                <Button name="Environmental Change Maker" style="button6" />
            </div>
            <p className="Build-and-manage-you ">Build and manage your network with PEEKaMEET</p>
        </div>
    )
}
export default LoginPageInfoLabels;
