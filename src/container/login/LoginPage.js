import React from 'react';
import Form from './form/Form';
import './LoginPage.css';
import BackgroundImage from './backgroundImage/BackgroundImage';
import LoginHeader from './loginHeader/LoginHeader'
import Footer from '../../component/footer/Footer'

import LoginPageInfoLabels from './LoginPageInfoLabels'

const LoginPage = () => {
    return (
        <div>
            <LoginHeader />
            <div className="outer-main-div">
                <div className="container">
                    <div className='main-container'>
                        <BackgroundImage />
                        <div className="formDiv">
                            <LoginPageInfoLabels />
                            <Form />
                           
                       <p>
                            <div  type="checkbox" id="remember" name="remember"/>
                            <label for="remember">Remember me      </label>
                      
                          <u className="rememberme">Forgot password</u></p>


                  <p className="bottom-para1">Don't have an account? <b><u>Sign UP</u></b> </p>
                        </div>
                    </div>
                  
                </div>
            </div>
            <Footer/>
        </div>

    )
}

export default LoginPage;