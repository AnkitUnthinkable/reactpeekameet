
import React, { Component } from 'react';
import './Form.css';
import Input from '../Input/Input';
import Button from '../button/Button';
import * as actionCreator from '../../../store/actions/authActions';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux'
import Spinner from '../../../UI/spinner/Spinner'
class RegisterForm extends Component {
    constructor() {
        super();
        this.state = {
            fields: {},
            errors: {},
            redirectCondition: false
        }
    };

    handleChange = (e) => {
        let fields = this.state.fields;
        fields[e.target.name] = e.target.value;
        this.setState({
            fields
        });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        if (this.validateForm()) {
            let fields = {};
            fields["email"] = "";
            fields["password"] = "";
            localStorage.setItem("email", this.state.fields.email);
            localStorage.setItem("password", this.state.fields.password);

            this.props.auth(this.state.fields.email, this.state.fields.password)

            this.setState({
                fields: fields,
                redirectCondition: true
            });
        }
    }

    validateForm = () => {
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;

        //email valiation
        //for empty fields
        if (!fields["email"]) {
            formIsValid = false;
            errors["email"] = "*Please enter your email-ID.";
        }

        //for other cases
        if (typeof fields["email"] !== "undefined") {
            let email = fields["email"];
            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
                errors["email"] = " "
            }
            else {
                formIsValid = false;
                errors["email"] = "email is not valid";
            }
        }

        //password check
        // to check empty fields
        if (!fields["password"]) {
            formIsValid = false;
            errors["password"] = "*Please enter your password.";
        }

        //to check length and number criteria 
        if (typeof fields["password"] !== "undefined") {
            let flag = false;
            let password = fields["password"];

            for (let i = 0; i < password.length; i++) {
                if (!isNaN(password.charAt(i))) {
                    flag = true;
                }
                if (flag === true) {
                    break;
                }
            }

            if (fields["password"].length < 8) {
                errors["password"] = "password must be 8 character long";
                formIsValid = false;
            }
            else {
                if (flag) {
                    errors["password"] = " "

                }
                else {
                    errors["password"] = "password must contain atleast one number";
                    formIsValid = false;
                }
            }
        }
        this.setState({
            errors: errors
        });
        return formIsValid;
    }
    render() {
        return (
            <div>
                {this.props.loading ? <Spinner /> : localStorage.getItem("token") ? <Redirect to="/user" /> :
                    <form onSubmit={this.handleSubmit}>
                        <Input type='text' label='Email' name='email' onChange={this.handleChange} error={this.state.errors.email} />
                        <Input type='password' label="Password" name='password' onChange={this.handleChange} error={this.state.errors.password} />
                        <Button style="submit-button" name="SIGN IN" />
                    </form>}
            </div>
        );
    }
}
const mapStateToPropps = state => {
    return {
        token: state.token,
        loading: state.loading,
        authenticated: state.authenticatd
    };
};
const maphDispatchToProps = dispatch => {
    return {
        auth: (email, password) =>
            dispatch(actionCreator.Authentication(email, password))
    };
};

export default connect(mapStateToPropps, maphDispatchToProps)(RegisterForm);
