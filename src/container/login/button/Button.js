import React from 'react';

const Button = (props) => {
    console.log("class-", props.className, "name-", props.name, "style-", props.style)
    return (

        <div className={props.className} >
            <button className={props.style}> {props.name} </button>
        </div>
    )
}

export default Button;