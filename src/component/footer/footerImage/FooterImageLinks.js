import React from 'react'

const FooterImageLinks = (props) => {
    return (
        <span>
            <img src={props.path} alt={props.alt}/>
        </span>
    )
}
export default FooterImageLinks