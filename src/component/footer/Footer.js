import React, { Component } from 'react';
import FooterLink from  './footerLinks/FooterLink' 
import FooterImageLinks from './footerImage/FooterImageLinks';
import './Footer.css';
import insta from '../../assets/images/footer icons/instagram-fill.png'
import youtube from '../../assets/images/footer icons/youtube-fill.png'
import facebook from '../../assets/images/footer icons/facebook-box-fill.png'
import twitter from '../../assets/images/footer icons/twitter-fill.png'

class Footer extends Component {
    render() {
        return (
            <div className="footer">
                <FooterLink textContent="PEEKaMeet" />
                <FooterLink textContent="FollowUs" />
                <FooterImageLinks path={insta} alt="insta"/>
                <FooterImageLinks path={youtube} alt="youtube"/>
                <FooterImageLinks path={facebook} alt="facebook"/>
                <FooterImageLinks path={twitter} alt = "twitter"/>
                <FooterLink textContent='FAQs' />
                <FooterLink textContent="Terms&Condition" />
                <FooterLink textContent="Privacy Policy" />
                <FooterLink textContent=" AboutUs" />
                <FooterLink textContent="Perks" />
                <FooterLink textContent="Blog" />
            </div>)
    }
}
export default Footer;