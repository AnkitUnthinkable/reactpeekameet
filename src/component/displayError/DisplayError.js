import React from 'react'

const DisplayError = () => {
    return (
        <div>
            <h1>Something went wrong</h1>
            <h2>Please refresh and try again</h2>
        </div>
    )
}
export default DisplayError;