import React, { Component } from "react";
import "./Notes.css";
import { withRouter } from "react-router";
import { connect } from "react-redux";
import * as actionCreators from "../../../store/actions/notesAction";
class Notes extends Component {
  state = { loading: false };

  edit = () => {
    this.props.history.push({
      pathname: "/user/notes/addnote",
      data: {
        noteId: this.props.noteId,
        noteText: this.props.NoteText,
        noteTime: this.props.NoteTime
      }
    });
  };

  view = () => {
    console.log("clicked view");
    console.log(this.props);
    this.props.history.push("/user/notes/addnote");
  };

  delet = () => {
    this.props.del(this.props.noteId);
    this.props.noteData.splice(this.props.inde, 1);
    this.props.history.push("/user/notes");
  };

  render() {
    return (
      <div className="Notes">
        <div className="NotesDetails">
          <p className="P1">{this.props.NoteText}</p>
          <p className="P2">{this.props.NoteTime}}</p>
        </div>
        <div className="Menubtn">
          <p onClick={this.edit}>Edit </p>
          <p onClick={this.delet}> Delete</p>
          <p onClick={this.view}>View</p>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return { noteData: state.noteData };
};
const mapDispatchToProps = dispatch => {
  return {
    del:(noteId, index) => dispatch(actionCreators.deleteNote(noteId, index))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Notes));
