import React, { Component } from "react";
import Coverpic from "./coverpic/CoverPic";
import ProfilePic from "./profilepic/Profilepic";
import "./UserProfile.css";
import playpicCircle from "../../assets/play_circle.svg";
import UserDetails from "./userDetails/UserDetails";
import Button from "../../UI/button/Button";
import shareIcon from "../../assets/share_icon.svg";
import editIcon from "../../assets/edit_icon.svg";
import Toggle from "./toggleDetails/Toggle";
import Details from "./details/Details";
import Notes from "./notes/Notes";
import AddNote from "./addNote/AddNote";
import { connect } from "react-redux";
import { UseContext } from "../../contextApi/ContextProvider";
import Spinner from "../../UI/spinner/Spinner";
import { Route, Switch } from "react-router";
import { Link } from "react-router-dom";

class UserProfile extends Component {
  static contextType = UseContext;
  render() {
    let userInfo = null;
    let notes;

    if (Object.keys(this.context).length !== 0) {
      if (this.props.noteData.length !== 0) {
        notes = this.props.noteData.map((note, index) => {
          return (
            <Notes
              NoteText={note.noteText}
              NoteTime={note.dateTime}
              key={note._id}
              noteId={note._id}
              inde={index}
            />
          );
        });
      }
      userInfo = (
        <div className="UserProfile">
          <div className="Pic">
            <Coverpic />
            <ProfilePic profileImage={this.context.profileImage} />
          </div>
          <img src={playpicCircle} alt='Play' className="Play" />
          <UserDetails
            userName={this.context.firstName + " " +this.context.lastName}
            userDesignation={this.context.jobTitle}
            userCompany={this.context.company}
          />
          <div className="ButtonSection">
            <Button buttonIcon={shareIcon}>Share</Button>
            <Button buttonIcon={editIcon}>Edit</Button>
          </div>
          <div className="ToggleSection">
            <Toggle details='Details' notes='Notes' />

            <Link to='/user/notes/addnote'>ADD NOTE</Link>
        
          </div>
          <hr/>
          <Switch>
            <Route
              exact
              path='/user'
              render={() => (
                <Details
                  quote='Inspired by the world and everything it has to offer.'
                  quoteDetails={this.context.bio}
                />
              )}
            />
            <Route path='/user/notes' render={() => notes} />
          </Switch>
        </div>
      );
    } else {
      userInfo = <Spinner />;
    }
    return <div>{userInfo}</div>;
  }
  addNoteToggleHandler = () => {
    this.setState({
      showAddNote: !this.state.showAddNote,
      disabled: !this.state.disabled
    });
  };
}

const mapStateToPropps = state => {
  return {
    token: state.token,
    id: state.userId,
    loading: state.loading,
    authenticated: state.authenticated,
    noteData: state.noteData
  };
};

export default connect(mapStateToPropps)(UserProfile);
