import React from "react";
import "./UserDetails.css";
const UserDetails = props => {
  return (
    <div className="UserDetails">
      <p className="P1">
        <strong>{props.userName}</strong>
      </p>
      <p className="P2">{props.userDesignation}</p>
      <p className="P3">{props.userCompany}</p>
    </div>
  );
};

export default UserDetails;
