import React, { Component } from "react";
import  "./AddNote.css";
import Element from "../../../UI/inputElement/InputElement";
import { UseContext } from "../../../contextApi/ContextProvider";
import { connect } from "react-redux";
import * as actionCreator from "../../../store/actions/notesAction";

class AddNote extends Component {
  componentDidMount() {
    if (this.props.location.data) {
      this.editNoteDataSetter(this.props.location.data.noteText);
    }
  }

  static contextType = UseContext;
  state = {
    Form: {
      date: {
        elementType: "input",
        elementConfig: {
          type: "date",
          placeholder: "Date"
        },
        value: "",
        validation: {
          required: true
        },
        valid: false,
        touched: false
      },

      time: {
        elementType: "input",
        elementConfig: {
          type: "time",
          placeholder: "Time"
        },
        value: "",
        validation: {
          required: true
        },
        valid: false,
        touched: false
      },

      note: {
        elementType: "textarea",
        elementConfig: {
          type: "textarea",
          placeholder: "Note"
        },
        value: "",
        validation: {
          required: true
        },
        valid: false,
        touched: false
      }
    }
  };

  render() {
    let Button = (
      <button type='submit' onClick={this.addNoteHandler}>
        Add Note
      </button>
    );
    if (this.props.location.data) {
      Button = (
        <button type='submit' onClick={this.editNoteHandler}>
          Edite Note
        </button>
      );
    }

    const formElementArray = [];
    for (let key in this.state.Form) {
      formElementArray.push({ id: key, config: this.state.Form[key] });
    }

    return (
      <div className="Addnote">
        <div className="NoteHeading">
          <p>Add Note</p>
        </div>
        {formElementArray.map(formElement => (
          <Element
            key={formElement.id}
            elementType={formElement.config.elementType}
            elementConfig={formElement.config.elementConfig}
            value={formElement.config.value}
            invalid={!formElement.config.valid}
            touched={formElement.config.touched}
            changed={event => this.inputHandler(event, formElement.id)}
          />
        ))}
        {Button}

        <button onClick={this.noteCancelHandler}>Cancel</button>
      </div>
    );
  }

  noteCancelHandler = () => {
    this.props.history.push("/user/notes");
  };

  inputHandler = (event, inputIdentifier) => {
    const updatedOrderForm = { ...this.state.Form };
    const updatedElement = { ...updatedOrderForm[inputIdentifier] };
    updatedElement.value = event.target.value;
    updatedElement.valid = this.checkValidity(
      updatedElement.value,
      updatedElement.validation
    );
    updatedElement.touched = true;
    updatedOrderForm[inputIdentifier] = updatedElement;
    this.setState({ Form: updatedOrderForm });
  };

  checkValidity = (value, rules) => {
    let isValid = true;
    if (rules.required) {
      isValid = value.trim() !== "" && isValid;
    }
    if (rules.minLength) {
      isValid = value.length >= rules.minLength && isValid;
    }
    if (rules.maxLength) {
      isValid = value.length <= rules.maxLength && isValid;
    }

    return isValid;
  };
  addNoteHandler = () => {
    let data = {
      createdFor: localStorage.getItem("id"),
      type: "note",
      noteText: this.state.Form.note.value,
      // dateTime: parseInt(this.props.notes)
      dateTime: 2
    };

    this.props.addNote(data, {
      headers: { Authorization: localStorage.getItem("token") }
    });

    this.props.noteData();
    this.props.history.push("/user/notes");
  };

  editNoteHandler = () => {
    this.props.editNote(
      this.props.location.data.noteId,
      {
        noteId: this.props.location.data.noteId,
        noteText: this.state.Form.note.value,
        dateTime:2
      },
      { headers: { Authorization: localStorage.getItem("token") } }
    );
    this.props.noteData();
    this.props.history.push("/user/notes");

  };

  editNoteDataSetter = noteText => {
    let updatedState = { ...this.state };
    updatedState.Form.note.value = noteText;
    this.setState({ Form: updatedState.Form });
  };
}

const mapStateToProps = state => {
  return {
    notes: state.noteData
  };
};

const mapDispatchToprops = dispatch => {
  return {
    noteData: () => dispatch(actionCreator.noteData()),
    addNote: (payLoad, header) =>
      dispatch(actionCreator.addNote(payLoad, header)),
    editNote: (noteId,payLoad,header) => dispatch(actionCreator.editNote(noteId,payLoad,header))
  };
};

export default connect(mapStateToProps, mapDispatchToprops)(AddNote);
