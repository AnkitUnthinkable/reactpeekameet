import React from "react";
import './BusinessAddress.css';

const BusinessAddress = props => {
  return (
    <div className="BusinessAddress">
      <p className="Heading"> Business Address</p>

      <p className="DetailedAddress">{props.BusinessAddress}
      </p>
    </div>
  );
};

export default BusinessAddress;
