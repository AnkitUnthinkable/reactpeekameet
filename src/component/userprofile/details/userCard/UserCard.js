import React from 'react';
import './UserCard.css';
import RoundContainer from '../../../../UI/roundContainer/RoundContainer';

const UserCard =(props)=>{
    const data=props.data;
    const roundContainer=(Object.keys(data).map(key=>{
        return <RoundContainer data={data[key]} key={key}/>
    }));
    return(
        <div className="UserCard">
            <p className="CardHeading">{props.CardHeading}</p>
            <section className="CardData">
                {roundContainer}
            </section>
        </div>
    );

};

export default UserCard;