import React, { useContext } from "react";
import invertedUpPic from "../../../assets/inverted_up.svg";
import invertedDownPic from "../../../assets/inverted_down.svg";
import  "./Details.css";
import UserCard from "./userCard/UserCard";
import ContactDetails from "./contactDetails/ContactDetils";
import BusinessAddress from "./address/BusinessAddress";
import email_icon from "../../../assets/email_icon.svg";
import phone_icon from "../../../assets/phone_icon.svg";
import weblink_icon from "../../../assets/weblink_icon.svg";
import { UseContext } from "../../../contextApi/ContextProvider";

const Details = props => {
  const detailsData = useContext(UseContext);
  const industry = { ...detailsData.industry };
  const organisationGroups = { ...detailsData.organisationGroups };
  const interestActivities = { ...detailsData.interestActivities };
  const alumni = { ...detailsData.alumni };
  const languages = { ...detailsData.languages };

  let industryCard = null;
  let organisationGroupsCard = null;
  let interestActivitiesCard = null;
  let alumniCard = null;
  let languagesCard = null;

  if (Object.keys(industry).length !== 0) {
    industryCard = <UserCard CardHeading='Industry(s)' data={industry} />;
  }
  
  if (Object.keys(organisationGroups).length !== 0) {
    organisationGroupsCard = (
      <UserCard CardHeading='Organisations and Groups' data={organisationGroups} />
    );
  }
  if (Object.keys(interestActivities).length !== 0) {
    interestActivitiesCard = (
      <UserCard
        CardHeading='Interest And activities'
        data={interestActivities}
      />
    );
  }
  if (Object.keys(alumni).length !== 0) {
    alumniCard = <UserCard CardHeading='Alumni' data={alumni} />;
  }
  if (Object.keys(languages).length !== 0) {
    languagesCard = <UserCard CardHeading='Languages' data={languages} />;
  }

  return (
    <div className="Details">
      <div className="Quote">
        <img src={invertedUpPic} alt='inverted comma' />
        <p>{props.quote} </p>
        <img src={invertedDownPic} alt='inverted comma' />
      </div>
      <div>
        <section className="QuoteDetails">{props.quoteDetails}</section>
      </div>
      {industryCard}
      {organisationGroupsCard}
      {interestActivitiesCard}
      {alumniCard}
      {languagesCard}

      <ContactDetails
        contactDetails={detailsData.businessPhone}
        contactDetailIcon={phone_icon}
      />
      <ContactDetails
        contactDetails={detailsData.email}
        contactDetailIcon={email_icon}
      />
      <ContactDetails
        contactDetails={detailsData.website}
        contactDetailIcon={weblink_icon}
      />
      <BusinessAddress
        company={detailsData.company}
        BusinessAddress={detailsData.businessAddress}
      />
    </div>
  );
};

export default Details;
