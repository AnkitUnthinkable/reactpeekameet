import React from "react";
import NavigationItem from "../navigationItem/NavigationItem";
import navigationItems from '../../../constant/NavigationItems';

const NavigationItems = () => {
//creating List of all the items
  const items = Object.keys(navigationItems).map(key => {
    return (
      <NavigationItem image={navigationItems[key].imagePath} key={key}>
        {navigationItems[key].message}
      </NavigationItem>
    );
  });
  return <ul>{items}</ul>;
};

export default NavigationItems;
