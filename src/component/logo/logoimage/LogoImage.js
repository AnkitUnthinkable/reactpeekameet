import React from "react";
import logoimage from '../../../assets/peekameet.png';

const LogoImage = () => {
  return (
    <img src={logoimage} alt='Logo'/> 
   );
};

export default LogoImage;
