import React from "react";
import "./LogoTitle.css";

const LogoTitle = props => {
  return <div className="Peekameet">{props.children} </div>;
};
export default LogoTitle;
