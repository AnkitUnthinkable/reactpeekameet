import React from "react";
import Logoimage from "./logoimage/LogoImage";
import Logotitle from "./logotitle/LogoTitle";
import      '../toolbar/Toolbar.css';
import {withRouter} from 'react-router-dom'

const Logo = () => {
  return (
    <div className="Title">
      <Logoimage />
      <Logotitle>PEEKaMEET</Logotitle>
    </div>
  );
};

export default withRouter (Logo);
