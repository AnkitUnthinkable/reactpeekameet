import React from "react";
import Toolbar from "../component/toolbar/Toolabar";
import UserProfile from "../component/userprofile/Userprofile";
import "./Layout.css";
import { connect } from "react-redux";
import ContextProvider from "../contextApi/ContextProvider";
import {  Route } from "react-router";
import AddNote from "../component/userprofile/addNote/AddNote";
import Footer from "../component/footer/Footer";


const Layout = props => {
  const logouthandler = () => {
    localStorage.removeItem("token");
    localStorage.removeItem("id");
    props.history.replace("/");
  };
  return (
    <div>
      <Toolbar logout={logouthandler} />
      <main>
        <ContextProvider>
          <Route exact path='/user' component={UserProfile} />
         
          <Route exact path='/user/notes' component={UserProfile} />

          <Route path='/user/notes/addnote' component={AddNote} />

        </ContextProvider>
      </main>
      <Footer/>
    </div>
  );
};

const mapStatetoProps = state => {
  return {
    authenticated: state.authenticated
  };
};

export default connect(mapStatetoProps)(Layout);




//import AddNote from "../component/userprofile/addNote/AddNote";

//<Route exact path='/user/notes' component={UserProfile} />

//<Route path='/user/notes/addnote' component={AddNote} />