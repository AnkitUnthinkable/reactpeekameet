import React from 'react';
import './header.css';
import Home from '../../assets/images/icons/Home.png';
import Contact from '../../assets/images/icons/contact.png';
import Scan from '../../assets/images/icons/credit-card-scan.png';
import Message from '../../assets/images/icons/message.png';
import Movement from '../../assets/images/icons/Movement.png';
import Notification from '../../assets/images/icons/Notification.png';
import logo from '../../assets/images/icons/logo.png'

const header = () => {
    return (
        <nav class="navbar navbar-expand-sm bg-success navbar-dark">
            <ul class="navbar-nav">
                <li class="nav-item active">
                <img src={logo} alt=" "  height='50px'/>
                    <a class="nav-link" href="#">PEEKaMEET</a>
                </li>
                <li class="nav-item">
                    <img src={Home} alt=" " />
                    <a class="nav-link" href="#">HOME</a>

                </li>
                <li class="nav-item">
                    <img src={Contact} alt=" " />
                    <a class="nav-link" href="#">CONTACTS</a>
                </li>
                <li class="nav-item">
                    <img src={Message} alt=" " />
                    <a class="nav-link " href="#">MESSAGE</a>
                </li>
                <li class="nav-item">
                    <img src={Scan} alt=" " />
                    <a class="nav-link" href="#">SCAN</a>
                </li>
                <li class="nav-item">
                    <img src={Movement} alt=" " />
                    <a class="nav-link " href="#">THE MOVEMENT</a>
                </li>
                <li class="nav-item">
                    <img src={Notification} alt=" " />
                    <a class="nav-link " href="#">NOTIFICATION</a>
                </li>
            </ul>
        </nav>


    );
}

export default header;